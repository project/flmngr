## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration

## INTRODUCTION

Flmngr is a file manager with an image editor aboard.

It will help you to upload, choose and manage files and also edit your images.
This module includes **CKEditor 4** and **CKEditor 5** plugins so will perfectly integrate into your
Drupal installation.

Official website: [flmngr.com](https://flmngr.com)

## REQUIREMENTS

There are no special requirements, except CKEditor 4 or CKEditor 5 which is already installed by default.
Flmngr acts as its add-on for CKEditor.

## INSTALLATION

 - [Drupal 8 installation manual](https://flmngr.com/doc/install-drupal8-module)
 - [Drupal 9 installation manual](https://flmngr.com/doc/install-drupal9-module)
 - [Drupal 10 installation manual](https://flmngr.com/doc/install-drupal10-module)

## CONFIGURATION

The module comes already preconfigured to be used after installation.
If you want to change any options, do it as usual for the text format you need on:

  `Configuration > Content authoring > Text formats`

choose and open the text format you want to configure, then will see all
Flmngr available options inside CKEditor configuration widget.
Press "Save" after editing something there.

## MAINTAINERS

- [Dmitriy Komarov](https://drupal.org/u/dmitriy-komarov)
- [Olga Tarkova](https://www.drupal.org/u/olga-tarkova)